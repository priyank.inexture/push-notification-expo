import React from 'react'
import AppContainer from './AppContainer'
import { GlobalProvider } from './context/globalContext'
import { useFonts } from 'expo-font';

function App() {
  const [loaded] = useFonts({
    nunitoLight: require('./assets/fonts/Nunito-Light.ttf'),
    nunitoRegular: require('./assets/fonts/Nunito-Regular.ttf'),
    nunitoSemiBold: require('./assets/fonts/Nunito-SemiBold.ttf'),
    nunitoBold: require('./assets/fonts/Nunito-Bold.ttf'),
  });
  if (!loaded) {
    return null;
  }
  return (
    <GlobalProvider>
      <AppContainer />
    </GlobalProvider>
  )
}

export default App
