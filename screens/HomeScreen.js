import React, { useRef, useEffect } from 'react'
import { Animated, View, Text, Button, FlatList, StatusBar, Dimensions, StyleSheet } from 'react-native'
import { useGlobalStateContext } from '../context/globalContext';
import * as Notifications from 'expo-notifications';
import Article from '../components/Article';
const { height, width } = Dimensions.get('window')
function HomeScreen({ navigation }) {
    const { articles, notification, expoPushToken } = useGlobalStateContext();
    const scrollY = useRef(new Animated.Value(0)).current;
    const opacity = scrollY.interpolate({
        inputRange: [-1, 0, height * 0.02],
        outputRange: [1, 1, 0],
    });
    ;
    useEffect(() => {
        handleLocalNotification();
    }, [])
    useEffect(() => {
        if (notification) navigation.navigate("Notification")
    }, [notification])

    return (
        <View
            style={{
                // flex: 1,
                // alignItems: 'center',
                // marginTop: StatusBar.currentHeight,
                // justifyContent: 'center'
            }}>
            {/* <Text>Your expo push token: {expoPushToken}</Text>
            <Text>NOTIFICATION {notification ? 'true' : 'false'}</Text>
            <Button
                title="Press to Send Notification"
                // onPress={async () => {
                //     await sendPushNotification(expoPushToken);
                // }}
                onPress={() => handleLocalNotification()}
            /> */}
            <Animated.Text style={[styles.text, { opacity }]}>Articles</Animated.Text>
            <FlatList
                onScroll={Animated.event([
                    {
                        nativeEvent: {
                            contentOffset: {
                                y: scrollY,
                            },
                        },
                    },
                ])}

                data={articles}
                contentContainerStyle={{
                    paddingVertical: StatusBar.currentHeight * 2.5
                }}
                keyExtractor={(item) => item.key.toString()}
                renderItem={({ item, index }) => <Article {...item} id={index} navigation={navigation} />}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    text: {
        ...StyleSheet.absoluteFill,
        marginTop: StatusBar.currentHeight * 1.5,
        marginHorizontal: width * 0.03,
        fontFamily: 'nunitoBold',
        fontSize: 28
    }
})
function handleLocalNotification() {
    const localNotification = {
        title: 'New Article',
        body: 'Your favorite author posted a new article in a while!',
        data: {
            source: {
                id: "the-wall-street-journal",
                name: "The Wall Street Journal"
            },
            author: "Nina Trentmann",
            title: "Pinterest CFO Looks to Ramp Up Spending on Marketing, Product Investments",
            description: "The image-sharing platform expects the number of U.S. users to plateau as the pandemic abates",
            url: "https://www.wsj.com/articles/pinterest-cfo-looks-to-ramp-up-spending-on-marketing-product-investments-11619570245",
            urlToImage: "https://images.wsj.net/im-330866/social",
            publishedAt: "2021-04-28T00:41:49Z",
            content: "Pinterest Inc.s\r\nfinance chief plans to spend more money on marketing to offset a potential slowdown in activity on its image-sharing platform as the U.S. economy reopens.The San Francisco-based comp… [+2861 chars]"
        }

    };

    Notifications.scheduleNotificationAsync({
        content: localNotification,
        trigger: {
            seconds: 5,
        },
    });
}
// Can use this function below, OR use Expo's Push Notification Tool-> https://expo.io/notifications
async function sendPushNotification(expoPushToken) {
    const message = {
        to: expoPushToken,
        sound: 'default',
        title: 'Original Title',
        body: 'And here is the body!',
        data: { someData: 'goes here' },
    };

    await fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Accept-encoding': 'gzip, deflate',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(message),
    });


}

export default HomeScreen
