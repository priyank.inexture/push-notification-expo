import React, { useEffect } from 'react'
import { View, Text, Dimensions, Image, Button } from 'react-native'
import { ADD_NEW_ARTICLE_ACTION, UPDATE_NOTIFICATION } from '../context/ActionType';
import { useGlobalDispatchContext, useGlobalStateContext } from '../context/globalContext';
const { width, height } = Dimensions.get('window');
function NotificationScreen({ navigation }) {
    const { notification } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext()
    useEffect(() => {
        return () => {
            // dispatch({ type: UPDATE_NOTIFICATION, notification: false })
        }
    }, [])
    return (
        <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            paddingHorizontal: width * 0.1
        }}>
            <Text
                style={{
                    textAlign: 'center',
                    fontFamily: 'nunitoSemiBold',
                    fontSize: 20
                }}
            >{notification && notification.request.content.body || "Alert"}</Text>
            {/* <Text>{JSON.stringify(notification)}</Text> */}
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                {
                    notification && <>
                        <Text style={{
                            fontFamily: 'nunitoRegular',
                            textAlign: 'justify',
                            marginVertical: height * 0.025,
                        }}>{notification && notification && notification.request.content.data.title}</Text>
                        <Image style={{
                            width: width * 0.5,
                            height: width * 0.5,
                            marginVertical: height * 0.05,
                            resizeMode: 'cover'
                        }}
                            source={{ uri: notification && notification.request.content.data.urlToImage }}
                        />
                        <Button title="Explore" onPress={() => {
                            dispatch({ type: UPDATE_NOTIFICATION, notification: false })
                            navigation.navigate('Detail', { ...notification.request.content.data })
                        }
                        } />
                    </>
                }
            </View>
        </View >
    )
}

export default NotificationScreen
