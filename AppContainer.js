import 'react-native-gesture-handler';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';
import { createSharedElementStackNavigator } from 'react-navigation-shared-element';
import { createAppContainer } from 'react-navigation'
import React, { useEffect, useRef } from 'react';
import { Platform } from 'react-native';
import { ADD_NEW_ARTICLE_ACTION, UPDATE_EXPO_TOKEN_ACTION, UPDATE_NOTIFICATION } from './context/ActionType';
import { useGlobalDispatchContext, useGlobalStateContext } from './context/globalContext';
import HomeScreen from './screens/HomeScreen';
import NotificationScreen from './screens/NotificationScreen';
import DetailScreen from './screens/DetailScreen';

Notifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: false,
        shouldSetBadge: false,
    }),
});
// const Stack = createStackNavigator();
const stackNavigator = createSharedElementStackNavigator(
    {
        Home: HomeScreen,
        Detail: DetailScreen,
        Notification: NotificationScreen,
    },
    {
        initialRouteName: 'Home',
        headerMode: 'none',
        defaultNavigationOptions: {
            // headerBackTitleVisible: false,
            cardStyleInterpolator: ({ current: { progress } }) => {
                return {
                    cardStyle: {
                        opacity: progress
                    }
                };
            }
        }
    }
);
const AppContainerNavigator = createAppContainer(stackNavigator);
export default function AppContainer() {
    const dispatch = useGlobalDispatchContext();
    const { notification } = useGlobalStateContext();
    const notificationListener = useRef();
    const responseListener = useRef();
    useEffect(() => {
        // console.log(ArticleData);
        registerForPushNotificationsAsync().then(token => dispatch({ type: UPDATE_EXPO_TOKEN_ACTION, token }));

        // This listener is fired whenever a notification is received while the app is foregrounded
        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
            dispatch({ type: UPDATE_NOTIFICATION, notification: notification })
            dispatch({ type: ADD_NEW_ARTICLE_ACTION, article: notification.request.content.data || null })
        });

        // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
            console.log('clicked response');
            console.log(response);
            try {
                dispatch({ type: UPDATE_NOTIFICATION, notification: response.notification });
            } catch (error) {
                console.log(error)
            }
            // navigation.navigate("Notification");
        });

        return () => {
            Notifications.removeNotificationSubscription(notificationListener.current);
            Notifications.removeNotificationSubscription(responseListener.current);
        };
    }, []);

    return (
        // <NavigationContainer>
        //     <Stack.Navigator initialRouteName={"Home"}>
        //         <Stack.Screen name="Home" component={HomeScreen} />
        //         <Stack.Screen name="Notification" component={NotificationScreen} />
        //     </Stack.Navigator>
        // </NavigationContainer>
        <NavigationContainer >
            <AppContainerNavigator />
        </NavigationContainer>
    );
}

async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
        const { status: existingStatus } = await Notifications.getPermissionsAsync();
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            const { status } = await Notifications.requestPermissionsAsync();
            finalStatus = status;
        }
        if (finalStatus !== 'granted') {
            alert('Failed to get push token for push notification!');
            return;
        }
        token = (await Notifications.getExpoPushTokenAsync()).data;
        // console.log(token);
    } else {
        alert('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
        Notifications.setNotificationChannelAsync('default', {
            name: 'default',
            importance: Notifications.AndroidImportance.MAX,
            vibrationPattern: [0, 250, 250, 250],
            lightColor: '#FF231F7C',
        });
    }

    return token;
}